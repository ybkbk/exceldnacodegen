﻿using System.IO;
using ExcelDnaCodeGen.Generators;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Metadata.Types;
using ExcelDnaCodeGen.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Tests
{
	[TestClass]
	public class Integration_CppGeneratorAndRule
	{
		[TestMethod]
		public void RunSampleTest()
		{
			var rule = new CppCliToNativeRule();
			var generator = new CppNativeGenerator(rule);
			var metadata = new CppCliToNativeMetadata();
			metadata.Class = new ClassMetadata {Name = "Calendar"};
			var method = new MethodMetadata();
			method.Name = "IsTradeDate";
			method.ReturnValue = new ParameterMetadata() {ParameterType = DataType.Boolean};
			method.Parameters.Add(new ParameterMetadata {ParameterType = DataType.Integer, Name = "currentDate"});
			metadata.Class.Methods.Add(method);

			var fileInfo = generator.Generate(metadata);

			Assert.AreEqual(fileInfo.DirectoryName, "cpp_native");
			Assert.AreEqual(fileInfo.FileName, "Calendar.cpp");
			Assert.AreEqual(fileInfo.Content, File.ReadAllText("CppNativeGeneratorTestResult.txt"));
		}
	}
}
