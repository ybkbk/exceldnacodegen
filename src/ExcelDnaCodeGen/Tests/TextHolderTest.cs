﻿using ExcelDnaCodeGen.Parsers.Vba;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class TextHolderTest
	{
		[TestMethod]
		public void GetNextPart_NormalText_ReturnNamespace()
		{
			var holder = new TextHolder("[Namespace(\"my.Client\")]global static function asCalendarIsTradeDate");
			var result = holder.GetNextPart();
			Assert.AreEqual("[Namespace(\"my.Client\")]", result);
		}

		[TestMethod]
		public void GetNextPart_NormalText_ReturnWord()
		{
			var holder = new TextHolder("test text");
			var result = holder.GetNextPart();
			Assert.AreEqual("test", result);
		}

		[TestMethod]
		public void GetNextPart_NormalTextWithSpaces_ReturnNamespace()
		{
			var holder = new TextHolder("[Description(\"Full name of the calendar\")] ByVal");
			var result = holder.GetNextPart();
			Assert.AreEqual("[Description(\"Full name of the calendar\")]", result);
		}

		[TestMethod]
		public void GetNextPart_OneWord_ReturnWord()
		{
			var holder = new TextHolder("test");
			var result = holder.GetNextPart();
			Assert.AreEqual("test", result);
		}

		[TestMethod]
		public void GetNextPart_TextWithBreak_ReturnWord()
		{
			var holder = new TextHolder(@"test
								          text");
			var result = holder.GetNextPart();
			Assert.AreEqual("test", result);

			holder.Move(result.Length);
			result = holder.GetNextPart();
			Assert.AreEqual("text", result);
		}

		[TestMethod]
		public void GetNextPart_EndLine_ReturnEmpty()
		{
			var holder = new TextHolder("test");
			holder.Move(4);
			var result = holder.GetNextPart();
			Assert.AreEqual("", result);
		}
	}
}
