﻿[WrappedClass("Calendar")]
[WrappedCall("IsTradeDate")]
function asCalendarIsTradeDate(
   calendarName as String,
   date as Object) 
   as Object
end
