﻿using System;
using ExcelDnaCodeGen.Generators;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;

namespace Tests
{
	[TestClass]
	public class CppNativeGeneratorTests
	{
		[TestMethod]
		public void Generate_NullArg_ThrowsArgumentExcepion()
		{
			//Arrange
			var mockRule = new Mock<CppCliToNativeRule>();
			CppNativeGenerator generator = new CppNativeGenerator(mockRule.Object);

			//Act & Assert
			ExceptionAssert.Throws<ArgumentNullException>(() => generator.Generate(null));
		}

		[TestMethod]
		public void Generate_NullClass_ThrowsArgumentExcepion()
		{
			//Arrange
			var mockRule = new Mock<CppCliToNativeRule>();
			CppNativeGenerator generator = new CppNativeGenerator(mockRule.Object);

			//Act & Assert
			ExceptionAssert.Throws<ArgumentNullException>(() => generator.Generate(new CppCliToNativeMetadata()));
		}
	}
}
