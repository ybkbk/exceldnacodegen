﻿using System;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Metadata.Types;
using ExcelDnaCodeGen.Parsers;
using ExcelDnaCodeGen.Parsers.Vba;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class VbaParserTest
	{
		[TestMethod]
		public void GetBasicMetadata_Standart()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/ClientExcelDNA.txt");
			Assert.AreEqual("ClientExcelDNA", meta.Class.Name);

			Assert.AreEqual(1, meta.Class.Methods.Count);
			ValidateMethodDeclaration(meta.Class.Methods[0], "asCalendarIsTradeDate", true, "Calendar", "IsTradeDate", "This finction checks is the given date is available for trade", Scope.Public);

			Assert.AreEqual(2, meta.Class.Methods[0].Parameters.Count);
			ValidateParam(meta.Class.Methods[0].Parameters[0], "calendarName", null, DataType.String, ParameterPassingMechanism.ByVal, "Full name of the calendar", null);
			ValidateParam(meta.Class.Methods[0].Parameters[1], "date", DataType.Integer, DataType.Object, ParameterPassingMechanism.ByRef, "Date to check", null);

			ValidateParam(meta.Class.Methods[0].ReturnValue, null, null, DataType.Object, ParameterPassingMechanism.ByRef, "Boolean value, represented as string or Exception message", null);
		}

		[TestMethod]
		public void GetBasicMetadata_OnlyRequiredAttributes()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/BasicMeta.txt");
			Assert.AreEqual("BasicMeta", meta.Class.Name);

			Assert.AreEqual(1, meta.Class.Methods.Count);
			ValidateMethodDeclaration(meta.Class.Methods[0], "asCalendarIsTradeDate", false, "Calendar", "IsTradeDate", null, Scope.Public);

			Assert.AreEqual(2, meta.Class.Methods[0].Parameters.Count);
			ValidateParam(meta.Class.Methods[0].Parameters[0], "calendarName", null, DataType.String, ParameterPassingMechanism.ByRef, null, null);
			ValidateParam(meta.Class.Methods[0].Parameters[1], "date", null, DataType.Object, ParameterPassingMechanism.ByRef, null, null);

			ValidateParam(meta.Class.Methods[0].ReturnValue, null, null, DataType.Object, ParameterPassingMechanism.ByRef, null, null);
		}

		[TestMethod]
		public void GetBasicMetadata_OnlyOneDescription()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/OnlyOneDescription.txt");
			Assert.AreEqual("OnlyOneDescription", meta.Class.Name);

			Assert.AreEqual(1, meta.Class.Methods.Count);
			ValidateMethodDeclaration(meta.Class.Methods[0], "asCalendarIsTradeDate", false, "Calendar", "IsTradeDate", null, Scope.Public);

			Assert.AreEqual(2, meta.Class.Methods[0].Parameters.Count);
			ValidateParam(meta.Class.Methods[0].Parameters[0], "calendarName", null, DataType.String, ParameterPassingMechanism.ByRef, null, null);
			ValidateParam(meta.Class.Methods[0].Parameters[1], "date", null, DataType.Object, ParameterPassingMechanism.ByRef, "test", null);

			ValidateParam(meta.Class.Methods[0].ReturnValue, null, null, DataType.Object, ParameterPassingMechanism.ByRef, null, null);
		}

		[TestMethod]
		public void GetBasicMetadata_ThreeArguments()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/ThreeArguments.txt");

			Assert.AreEqual(3, meta.Class.Methods[0].Parameters.Count);
			ValidateParam(meta.Class.Methods[0].Parameters[2], "param", null, DataType.Integer, ParameterPassingMechanism.ByRef, null, null);
		}

		[TestMethod]
		public void GetBasicMetadata_Sub()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/Sub.txt");

			Assert.AreEqual(0, meta.Class.Methods[0].Parameters.Count);
			Assert.IsNull(meta.Class.Methods[0].ReturnValue);
		}

		[TestMethod]
		public void GetBasicMetadata_Array()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/Array.txt");

			Assert.AreEqual(2, meta.Class.Methods[0].Parameters.Count);
			ValidateArrayParam(meta.Class.Methods[0].Parameters[0], "arr", null, DataType.Array, ParameterPassingMechanism.ByVal, null, null, 1, DataType.String);
			ValidateArrayParam(meta.Class.Methods[0].Parameters[1], "arr1", null, DataType.Array, ParameterPassingMechanism.ByRef, null, null, 2, DataType.Integer);
		}

		[TestMethod]
		public void GetBasicMetadata_DefaultValue()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/DefaultValue.txt");

			Assert.AreEqual(1, meta.Class.Methods[0].Parameters.Count);
			ValidateParam(meta.Class.Methods[0].Parameters[0], "atr", null, DataType.Integer, ParameterPassingMechanism.ByRef, "Optional", "1");

			Assert.IsNull(meta.Class.Methods[0].ReturnValue);
		}

		[TestMethod]
		public void GetBasicMetadata_ThreeMethods()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/ThreeMethods.txt");

			Assert.AreEqual(3, meta.Class.Methods.Count);
			ValidateMethodDeclaration(meta.Class.Methods[0], "asCalendarIsTradeDate", false, "Calendar", "IsTradeDate", null, Scope.Public);
			Assert.IsNotNull(meta.Class.Methods[0].ReturnValue);
			ValidateMethodDeclaration(meta.Class.Methods[1], "asCalendarIsWeekend", true, "Calendar", "IsWeekend", null, Scope.Private);
			Assert.IsNotNull(meta.Class.Methods[1].ReturnValue);
			ValidateMethodDeclaration(meta.Class.Methods[2], "asCalendarUpdate", false, "Calendar", "Update", null, Scope.Public);
			Assert.IsNull(meta.Class.Methods[2].ReturnValue);

			ValidateParam(meta.Class.Methods[2].Parameters[0], "flag", null, DataType.Boolean, ParameterPassingMechanism.ByRef, null, "true");
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void GetBasicMetadata_InvalidStructure_Exception()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/InvalidStructure.txt");
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void GetBasicMetadata_MissedAttribute_Exception()
		{
			var parser = new VbaParser();
			var meta = parser.GetBasicMetadata(@"MetadataTemplates/MissedAttributes.txt");
		}

		#region Helpers
		private static void ValidateMethodDeclaration(
			MethodMetadata method,
			string name,
			bool isStatic,
			string wrappedClass,
			string wrappedCall,
			string description,
			Scope scope)
		{
			Assert.IsNotNull(method);
			Assert.AreEqual(name, method.Name);
			Assert.AreEqual(isStatic, method.IsStatic);
			Assert.AreEqual(wrappedClass, method.WrappedClass);
			Assert.AreEqual(wrappedCall, method.WrappedCall);
			Assert.AreEqual(description, method.Description);
			Assert.AreEqual(scope, method.Scope);
		}

		private static void ValidateParam(
			ParameterMetadata param,
			string name,
			DataType? originalType,
			DataType type,
			ParameterPassingMechanism pass,
			string description,
			string def)
		{
			Assert.IsNotNull(param);
			Assert.AreEqual(name, param.Name);
			Assert.AreEqual(originalType, param.OriginalType);
			Assert.AreEqual(type, param.ParameterType);
			Assert.AreEqual(pass, param.PassingMechanism);
			Assert.AreEqual(description, param.Description);
			Assert.AreEqual(def, param.DefaultValue);
		}

		private static void ValidateArrayParam(
			ParameterMetadata param,
			string name,
			DataType? originalType,
			DataType type,
			ParameterPassingMechanism pass,
			string description,
			string def,
			int demCount,
			DataType elemType)
		{
			ValidateParam(param, name, originalType, type, pass, description, def);
			Assert.IsTrue(param is ArrayParameter);
			var array = param as ArrayParameter;
			Assert.AreEqual(demCount, array.DementialsCount);
			Assert.AreEqual(elemType, array.ElementType);
		}
		#endregion
	}
}
