﻿namespace ExcelDnaCodeGen
{
	public class FileInfo
	{
		public string DirectoryName { get; set; }

		public string FileName { get; set; }

		public string Content { get; set; }
	}
}
