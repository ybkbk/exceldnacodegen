﻿using ExcelDnaCodeGen.Generators;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Parsers;
using Microsoft.Practices.Unity;

namespace ExcelDnaCodeGen
{
	class Program
	{
		private static readonly IUnityContainer IoC = new UnityContainer().Bootstrap();

		static void Main(string[] args)
		{
			var provider = IoC.Resolve<IBasicMetadataProvider>();
			var startGenerator = IoC.Resolve<IGenerator<VbaToDnaMetadata>>();
			startGenerator.Generate(provider.GetBasicMetadata("ClientExcelDNA.txt"));
		}
	}
}
