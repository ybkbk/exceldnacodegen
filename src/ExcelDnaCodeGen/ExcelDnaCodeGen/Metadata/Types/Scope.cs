﻿namespace ExcelDnaCodeGen.Metadata.Types
{
	public enum Scope
	{
		Public,
		Private,
		Internal,
		Protected
	}
}
