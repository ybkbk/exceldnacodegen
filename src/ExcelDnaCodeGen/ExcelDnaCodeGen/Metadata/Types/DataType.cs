﻿namespace ExcelDnaCodeGen.Metadata.Types
{
	// Data types specific for VBA. Possible not all of them we should support.
	public enum DataType
	{
		Boolean,
		Byte,
		Currency,
		Date,
		Decimal,
		Double,
		Integer,
		Long,
		LongLong,
		Object,
		Single,
		String,
		Variant,
		Array,
		UserDefined,
		WcfCallResult
	}
}
