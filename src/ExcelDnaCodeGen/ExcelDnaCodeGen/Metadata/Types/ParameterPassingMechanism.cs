﻿namespace ExcelDnaCodeGen.Metadata.Types
{
	public enum ParameterPassingMechanism
	{
		ByRef,
		ByVal
	}
}
