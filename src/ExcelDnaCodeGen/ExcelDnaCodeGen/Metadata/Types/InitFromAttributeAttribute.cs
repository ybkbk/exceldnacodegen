﻿using System;

namespace ExcelDnaCodeGen.Metadata.Types
{
	class InitFromAttributeAttribute : Attribute
	{
		public InitFromAttributeAttribute(string name, bool isRequired = false)
		{
			Name = name;
			IsRequired = isRequired;
		}

		public string Name { get; private set; }
		public bool IsRequired { get; private set; }
	}
}
