﻿using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Metadata
{
	public class ParameterMetadata
	{
		public string Name { get; set; }

		public DataType ParameterType { get; set; }

		public string DefaultValue { get; set; }

		public bool IsOptional
		{
			get { return DefaultValue != null; }
		}

		[InitFromAttribute("Description")]
		public string Description { get; set; }

		[InitFromAttribute("OriginalType")]
		public DataType? OriginalType { get; set; }

		public ParameterPassingMechanism PassingMechanism { get; set; }
	}
}
