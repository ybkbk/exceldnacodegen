﻿using System.Collections.Generic;
using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Metadata
{
	public class MethodMetadata : ScopedItem
	{
		public MethodMetadata()
		{
			Parameters= new List<ParameterMetadata>();
		}

		public string Name { get; set; }

		public ParameterMetadata ReturnValue { get; set; }

		public List<ParameterMetadata> Parameters { get; set; }

		[InitFromAttribute("WrappedClass", true)]
		public string WrappedClass { get; set; }

		[InitFromAttribute("WrappedCall", true)]
		public string WrappedCall { get; set; }

		[InitFromAttribute("Description")]
		public string Description { get; set; }
	}
}
