﻿using System.Collections.Generic;
using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Metadata
{
	public class ClassMetadata : ScopedItem
	{
		public ClassMetadata()
		{
			Methods = new List<MethodMetadata>();
		}

		public string Namespace { get; set; }

		public string Name { get; set; }

		public IList<MethodMetadata> Methods { get; set; }
	}
}
