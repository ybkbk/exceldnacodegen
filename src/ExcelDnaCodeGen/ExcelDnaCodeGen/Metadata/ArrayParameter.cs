﻿using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Metadata
{
	public class ArrayParameter : ParameterMetadata
	{
		public int DementialsCount { get; set; }

		public DataType ElementType { get; set; }
	}
}
