﻿using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Metadata
{
	public class ScopedItem
	{
		public bool IsStatic { get; set; }

		public Scope Scope { get; set; }
	}
}
