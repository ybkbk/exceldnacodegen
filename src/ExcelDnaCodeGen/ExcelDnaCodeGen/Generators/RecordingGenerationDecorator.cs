﻿using System.IO;
using ExcelDnaCodeGen.Metadata;

namespace ExcelDnaCodeGen.Generators
{
	public class RecordingGenerationDecorator<T> : IGenerator<T> 
		where T : MetadataBase
	{
		private readonly IGenerator<T> _decoratedGenerator;

		public RecordingGenerationDecorator(IGenerator<T> decoratedGenerator)
		{
			_decoratedGenerator = decoratedGenerator;
		}

		public FileInfo Generate(T inputMeadata)
		{
			var fileInfo = _decoratedGenerator.Generate(inputMeadata);

			var directory = Directory.CreateDirectory(fileInfo.DirectoryName);

			var filePath = Path.Combine(directory.FullName, fileInfo.FileName);

			File.WriteAllText(filePath, fileInfo.Content);

			return fileInfo;
		}
	}
}
