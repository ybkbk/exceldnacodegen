﻿using System;
using System.Linq;
using System.Text;
using ExcelDnaCodeGen.Infrastrucure;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Metadata.Types;
using ExcelDnaCodeGen.Rules;

namespace ExcelDnaCodeGen.Generators
{
	public class DnaGenerator : OneFollowedGenerator<VbaToDnaMetadata, DnaToWcfInterfaceMetadata>
	{
		private readonly VbaToDnaRule _rule;

		public DnaGenerator(VbaToDnaRule rule, IGenerator<DnaToWcfInterfaceMetadata> wcfInerfaceGenerator)
			: base(rule, wcfInerfaceGenerator)
		{
			_rule = rule;
		}

		public override DnaToWcfInterfaceMetadata TansformMetadata(VbaToDnaMetadata inputMeadata)
		{
			var classMeta = new ClassMetadata();
			foreach (var methodMetadata in inputMeadata.Class.Methods)
			{
				var method = new MethodMetadata
				             {
						Description = methodMetadata.Description,
						Name = methodMetadata.WrappedCall,
						Scope = methodMetadata.Scope,
						ReturnValue = new ParameterMetadata {ParameterType = DataType.Integer},
					};

				foreach (var parameterMetadata in methodMetadata.Parameters)
				{
					var param = new ParameterMetadata();
					if (parameterMetadata.ParameterType == DataType.Array)
					{
						var arrayParam = parameterMetadata as ArrayParameter;
						param = new ArrayParameter {DementialsCount = arrayParam.DementialsCount, ElementType = arrayParam.ElementType};
					}

					param.Name = parameterMetadata.Name;
					param.PassingMechanism = parameterMetadata.PassingMechanism;
					param.DefaultValue = parameterMetadata.DefaultValue;
					param.Description = parameterMetadata.Description;
					method.Parameters.Add(param);
				}

				classMeta.Methods.Add(method);
				classMeta.Name = methodMetadata.WrappedClass;
			}

			return new DnaToWcfInterfaceMetadata { Class = classMeta };
		}

		public override FileInfo Generate(VbaToDnaMetadata inputMeadata)
		{
			var template = _rule.GetClassTemplate();
			var classBuilder = new StringBuilder(template);
			var methodIndent = GetIndentForPlaceHolder(template, Constants.PlaceHolders.Method);
			var methods = BuildMethodsCode(inputMeadata);
			methods = string.Join(Environment.NewLine + methodIndent, methods.Split(new[] {Environment.NewLine}, StringSplitOptions.None));
			classBuilder.Replace(Constants.PlaceHolders.ClassName, inputMeadata.Class.Name)
						.Replace(Constants.PlaceHolders.Method, methods);

			return new FileInfo { DirectoryName = _rule.DirectoryName, FileName = _rule.GetFileName(inputMeadata.Class.Name), Content = classBuilder.ToString()};
		}

		#region Helpers methods

		private string BuildMethodsCode(VbaToDnaMetadata inputMeadata)
		{
			return string.Join(Environment.NewLine, inputMeadata.Class.Methods.Select(BuildMethodCode));
		}

		private string BuildMethodCode(MethodMetadata meta)
		{
			var template = _rule.GetMethodTemplate();
			var paramsValidationIndent = GetIndentForPlaceHolder(template, Constants.PlaceHolders.ParamsValidation);
			var wrappedParamsIndent = GetIndentForPlaceHolder(template, Constants.PlaceHolders.WrappedParams);
			var methodBuilder = new StringBuilder(_rule.GetMethodTemplate());
			methodBuilder.Replace(Constants.PlaceHolders.MethodDescription, ToDescription(meta))
						.Replace(Constants.PlaceHolders.ReturnValueDescription, ToDescription(meta.ReturnValue))
						.Replace(Constants.PlaceHolders.MethodName, meta.Name)
						.Replace(Constants.PlaceHolders.WrappedClass, meta.WrappedClass)
						.Replace(Constants.PlaceHolders.WrappedCall, meta.WrappedCall)
						.Replace(Constants.PlaceHolders.Scope, ToScope(meta))
						.Replace(Constants.PlaceHolders.ParamsComment, ToParamsComment(meta))
						.Replace(Constants.PlaceHolders.ReturnComment, ToReturnComment(meta))
						.Replace(Constants.PlaceHolders.ReturnType, ToReturnType(meta))
						.Replace(Constants.PlaceHolders.Params, ToParams(meta))
						.Replace(Constants.PlaceHolders.ParamsValidation, ToParamsValidation(meta, paramsValidationIndent))
						.Replace(Constants.PlaceHolders.WrappedParams, ToWrappedParams(meta, wrappedParamsIndent));

			return methodBuilder.ToString();
		}

		private string ToWrappedParams(MethodMetadata meta, string indent)
		{
			var argBuilder = new StringBuilder();
			foreach (var parameter in meta.Parameters)
			{
				if (argBuilder.Length != 0)
				{
					argBuilder.Append(Constants.Comma);
					argBuilder.Append(Environment.NewLine);
					argBuilder.Append(indent);
				}

				if (parameter.OriginalType.HasValue)
				{
					argBuilder.Append(ToTypedParser(parameter));
				}
				else
				{
					argBuilder.Append(
						_rule.GetCodeFormatForPlaceHolder(Constants.PlaceHolders.GeneralParser)
							.Replace(Constants.PlaceHolders.ParamName, parameter.Name));
				}
			}

			return argBuilder.ToString();
		}

		private string ToParamsValidation(MethodMetadata meta, string indent)
		{
			var validationBuilder = new StringBuilder();
			foreach (var parameter in meta.Parameters)
			{
				if (validationBuilder.Length > 0)
				{
					validationBuilder.Append(indent);
				}

				string format, argument;
				if (parameter.OriginalType.HasValue)
				{
					argument = ToTypedParser(parameter);
					format = _rule.GetValidatorForType(parameter.OriginalType.Value);
				}
				else
				{
					argument = parameter.Name;
					format = _rule.GetValidatorForType(parameter.ParameterType);
				}

				validationBuilder.AppendLine(
					format.Replace(Constants.PlaceHolders.ParamName, argument)
						.Replace(Constants.PlaceHolders.ParamDescription, parameter.Name));
			}

			return validationBuilder.ToString();
		}

		private string ToTypedParser(ParameterMetadata parameter)
		{
			return _rule.GetCodeFormatForPlaceHolder(Constants.PlaceHolders.TypedParser)
						.Replace(Constants.PlaceHolders.ParamName, parameter.Name)
						.Replace(Constants.PlaceHolders.OriginalType, _rule.GetType(parameter.OriginalType.Value));
		}

		private string ToParams(MethodMetadata meta)
		{
			var format = _rule.GetCodeFormatForPlaceHolder(Constants.PlaceHolders.Param);
			return string.Join(Constants.Comma,
								meta.Parameters.Select(
									p =>
									format.Replace(Constants.PlaceHolders.ParamDescription, ToDescription(p))
										.Replace(Constants.PlaceHolders.ParamName, p.Name)
										.Replace(Constants.PlaceHolders.ParamType, _rule.GetType(p.ParameterType))));
		}

		private string ToReturnType(MethodMetadata meta)
		{
			if (meta.ReturnValue == null)
			{
				return Constants.Void;
			}

			return _rule.GetType(meta.ReturnValue.ParameterType);
		}

		private string ToReturnComment(MethodMetadata meta)
		{
			if (meta.ReturnValue == null)
			{
				return string.Empty;
			}

			return
				_rule.GetCodeFormatForPlaceHolder(Constants.PlaceHolders.ReturnComment)
					.Replace(Constants.PlaceHolders.ReturnValueDescription, ToDescription(meta.ReturnValue));
		}

		private string ToParamsComment(MethodMetadata meta)
		{
			var format = _rule.GetCodeFormatForPlaceHolder(Constants.PlaceHolders.ParamsComment);
			return
				string.Concat(
					meta.Parameters.Select(
						p =>
						format.Replace(Constants.PlaceHolders.ParamName, p.Name)
							.Replace(Constants.PlaceHolders.ParamDescription, ToDescription(p))));
		}

		private string ToScope(MethodMetadata meta)
		{
			return string.Format("{0}{1}", meta.Scope, meta.IsStatic ? " static" : string.Empty);
		}

		private static string ToDescription(MethodMetadata metadata)
		{
			return string.IsNullOrEmpty(metadata.Description) ? metadata.Name : metadata.Description;
		}

		private static string ToDescription(ParameterMetadata metadata)
		{
			return string.IsNullOrEmpty(metadata.Description) ? metadata.Name : metadata.Description;
		}

		private static string GetIndentForPlaceHolder(string template, string placeHolder)
		{
			var parts = template.Split(new[] {Environment.NewLine}, StringSplitOptions.None);
			var line = parts.FirstOrDefault(p => p.Contains(placeHolder));
			if (line != null)
			{
				for (int i = 0; i < line.Length; i++)
				{
					if (!Char.IsWhiteSpace(line[i]))
					{
						return line.Substring(0, i);
					}
				}
			}

			return string.Empty;
		}

		#endregion
	}
}
