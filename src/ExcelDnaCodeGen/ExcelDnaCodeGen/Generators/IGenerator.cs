﻿using ExcelDnaCodeGen.Metadata;

namespace ExcelDnaCodeGen.Generators
{
	public interface IGenerator <in TInMetadata>
		where TInMetadata : MetadataBase
	{
		FileInfo Generate(TInMetadata inputMeadata);
	}
}
