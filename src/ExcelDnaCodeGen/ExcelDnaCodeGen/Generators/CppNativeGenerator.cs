﻿using ExcelDnaCodeGen.Rules;

namespace ExcelDnaCodeGen.Generators
{
	public class CppNativeGenerator : TrivialGenerator
	{
		public CppNativeGenerator(CppCliToNativeRule rule)
			: base(rule)
		{
		}
	}
}
