﻿using System;
using System.Linq;
using System.Text;
using ExcelDnaCodeGen.Infrastrucure;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Rules;

namespace ExcelDnaCodeGen.Generators
{
	public class TrivialGenerator : IGenerator<MetadataBase>
	{
		protected readonly IRule Rule;

		public TrivialGenerator(IRule rule)
		{
			Rule = rule;
		}

		public virtual FileInfo Generate(MetadataBase inputMeadata)
		{
			Ensure.NotNull(() => inputMeadata);
			Ensure.NotNull(() => inputMeadata.Class);

			var classTeplate = Rule.GetClassTemplate();
			var classSb = new StringBuilder(classTeplate);
			classSb.Replace(Constants.PlaceHolders.ClassName, inputMeadata.Class.Name); 

			var methodTemplate = classTeplate
				.Split(Environment.NewLine.ToCharArray())
				.First(x => x.Contains(Constants.PlaceHolders.Method));

			var alignment = methodTemplate.Substring(0, methodTemplate.IndexOf(Constants.PlaceHolders.Method));

			var methodsSb = new StringBuilder();

			if (inputMeadata.Class.Methods.Count > 0)
			{
				int i = 0;
				foreach (var methodMetadata in inputMeadata.Class.Methods)
				{
					var methodImplementation = GetMethodImplemetation(inputMeadata.Class, methodMetadata, alignment);

					methodsSb.Append(methodImplementation);
					if (++i != inputMeadata.Class.Methods.Count)
					{
						methodsSb.AppendLine();
						methodsSb.AppendLine();
					}
				}
			}

			classSb.Replace(methodTemplate, methodsSb.ToString());

			return new FileInfo
				   {
					   DirectoryName = Rule.DirectoryName,
					   FileName = Rule.GetFileName(inputMeadata.Class.Name),
					   Content = classSb.ToString()
				   };
		}

		protected virtual string GetMethodImplemetation(ClassMetadata classMetadata, MethodMetadata methodMetadata, string alignment)
		{
			var methodSb = new StringBuilder(Rule.GetMethodTemplate());

			methodSb.Replace(Constants.PlaceHolders.MethodName, methodMetadata.Name);
			methodSb.Replace(Constants.PlaceHolders.ClassName, classMetadata.Name);

			methodSb.Replace(Constants.PlaceHolders.ReturnType, Rule.GetType(methodMetadata.ReturnValue.ParameterType));

			var paramsString = string.Join(", ",
				methodMetadata.Parameters.Select(x => string.Format("{0} {1}", Rule.GetType(x.ParameterType), x.Name)));

			methodSb.Replace(Constants.PlaceHolders.Params, paramsString);

			var formalParamsString = GetFormalParams(methodMetadata);

			methodSb.Replace(Constants.PlaceHolders.FormalParams, formalParamsString);

			methodSb.Replace(Environment.NewLine, Environment.NewLine + alignment);
			methodSb.Insert(0, alignment);

			return methodSb.ToString();
		}

		protected virtual string GetFormalParams(MethodMetadata methodMetadata)
		{
			return string.Join(", ", methodMetadata.Parameters.Select(x => x.Name));
		}
	}
}
