﻿using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Rules;

namespace ExcelDnaCodeGen.Generators
{
	public abstract class OneFollowedGenerator<TInMetadata, TOutMetadata> : TrivialGenerator, IGenerator<TInMetadata> 
		where TInMetadata : MetadataBase
		where TOutMetadata : MetadataBase
	{
		private readonly IGenerator<TOutMetadata> _follower;

		protected OneFollowedGenerator(IRule rule, IGenerator<TOutMetadata> follower)
			: base(rule)
		{
			_follower = follower;
		}

		public virtual FileInfo Generate(TInMetadata inputMeadata)
		{
			return base.Generate(inputMeadata);
		}

		public abstract TOutMetadata TansformMetadata(TInMetadata inputMeadata);

		FileInfo IGenerator<TInMetadata>.Generate(TInMetadata inputMeadata)
		{
			var fileInfo = Generate(inputMeadata);

			var output = TansformMetadata(inputMeadata);

			_follower.Generate(output);

			return fileInfo;
		}
	}
}
