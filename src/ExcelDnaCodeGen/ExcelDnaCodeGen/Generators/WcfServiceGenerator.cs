﻿using System.Linq;
using ExcelDnaCodeGen.Infrastrucure;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Rules;

namespace ExcelDnaCodeGen.Generators
{
	public class WcfServiceGenerator : OneFollowedGenerator<WcfIterfaceToServiceMetadata, WcfServiceToCppCliMetadata>
	{
		public WcfServiceGenerator(WcfIterfaceToServiceRule rule, IGenerator<WcfServiceToCppCliMetadata> cppCliGenerator)
			: base(rule, cppCliGenerator)
		{
		}

		public override WcfServiceToCppCliMetadata TansformMetadata(WcfIterfaceToServiceMetadata inputMeadata)
		{
			foreach (var method in inputMeadata.Class.Methods)
			{
				if (method.Parameters.Count > 0)
				{
					method.Parameters.RemoveAt(0);
				}
			}

			return new WcfServiceToCppCliMetadata { Class = inputMeadata.Class };
		}

		protected override string GetMethodImplemetation(ClassMetadata classMetadata, MethodMetadata methodMetadata, string alignment)
		{
			var methodImpl = base.GetMethodImplemetation(classMetadata, methodMetadata, alignment);

			return methodMetadata.Parameters.Count > 0
				? methodImpl.Replace(Constants.PlaceHolders.FirstParam, methodMetadata.Parameters[0].Name) 
				: methodImpl;
		}

		protected override string GetFormalParams(MethodMetadata methodMetadata)
		{
			return string.Join(", ", methodMetadata.Parameters.Skip(1).Select(x => x.Name));
		}
	}
}
