﻿using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Rules;

namespace ExcelDnaCodeGen.Generators
{
	public class CppCliGenerator : OneFollowedGenerator<WcfServiceToCppCliMetadata, CppCliToNativeMetadata>
	{
		public CppCliGenerator(WcfServiceToCppCliRule rule, IGenerator<CppCliToNativeMetadata> cppNativeGenerator)
			: base(rule, cppNativeGenerator)
		{
		}

		public override CppCliToNativeMetadata TansformMetadata(WcfServiceToCppCliMetadata inputMeadata)
		{
			return new CppCliToNativeMetadata { Class = inputMeadata.Class };
		}
	}
}
