﻿using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Rules;

namespace ExcelDnaCodeGen.Generators
{
	public class WcfInterfaceGenerator : OneFollowedGenerator<DnaToWcfInterfaceMetadata, WcfIterfaceToServiceMetadata>
	{
		public WcfInterfaceGenerator(DnaToWcfInterfaceRule rule, IGenerator<WcfIterfaceToServiceMetadata> wcfServiceGenerator)
			: base(rule, wcfServiceGenerator)
		{
		}

		public override WcfIterfaceToServiceMetadata TansformMetadata(DnaToWcfInterfaceMetadata inputMeadata)
		{
			return new WcfIterfaceToServiceMetadata { Class = inputMeadata.Class };
		}
	}
}
