﻿using ExcelDnaCodeGen.Generators;
using ExcelDnaCodeGen.Infrastrucure;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Parsers;
using ExcelDnaCodeGen.Parsers.Vba;
using ExcelDnaCodeGen.Rules;
using Microsoft.Practices.Unity;

namespace ExcelDnaCodeGen
{
	internal static class Bootstrapper
	{
		public static IUnityContainer Bootstrap(this IUnityContainer ioC)
		{
			//Registering rules
			ioC.Instance<VbaToDnaRule>()
				.Instance<DnaToWcfInterfaceRule>()
				.Instance<WcfIterfaceToServiceRule>()
				.Instance<WcfServiceToCppCliRule>()
				.Instance<CppCliToNativeRule>()

			//Registering generators
				.DecoratorChain<IGenerator<VbaToDnaMetadata>>()
					.Decorator<DnaGenerator>()
					.Decorator<RecordingGenerationDecorator<VbaToDnaMetadata>>()
				.EndChain()
				.DecoratorChain<IGenerator<DnaToWcfInterfaceMetadata>>()
					.Decorator<WcfInterfaceGenerator>()
					.Decorator<RecordingGenerationDecorator<DnaToWcfInterfaceMetadata>>()
				.EndChain()
				.DecoratorChain<IGenerator<WcfIterfaceToServiceMetadata>>()
					.Decorator<WcfServiceGenerator>()
					.Decorator<RecordingGenerationDecorator<WcfIterfaceToServiceMetadata>>()
				.EndChain()
				.DecoratorChain<IGenerator<WcfServiceToCppCliMetadata>>()
					.Decorator<CppCliGenerator>()
					.Decorator<RecordingGenerationDecorator<WcfServiceToCppCliMetadata>>()
				.EndChain()
				.DecoratorChain<IGenerator<CppCliToNativeMetadata>>()
					.Decorator<CppNativeGenerator>()
					.Decorator<RecordingGenerationDecorator<CppCliToNativeMetadata>>()
				.EndChain()

			//Parser
				.RegisterType(typeof(IBasicMetadataProvider), typeof(VbaParser), null, new ContainerControlledLifetimeManager());

			//Other registrations

			return ioC;
		}
	}
}
