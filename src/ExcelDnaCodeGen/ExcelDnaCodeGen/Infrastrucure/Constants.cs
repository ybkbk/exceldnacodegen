﻿namespace ExcelDnaCodeGen.Infrastrucure
{
	internal static class Constants
	{
		internal static class PlaceHolders
		{
			public const string ClassName = "{ClassName}";

			public const string Method = "{Method}";

			public const string MethodName = "{MethodName}";

			public const string ReturnType = "{ReturnType}";

			public const string ReturnComment = "{ReturnComment}";

			public const string Params = "{Params}";

			public const string ParamsComment = "{ParamsComment}";

			public const string ReturnValueDescription = "{ReturnValueDescription}";

			public const string MethodDescription = "{MethodDescription}";

			public const string WrappedClass = "{WrappedClass}";

			public const string Scope = "{Scope}";

			public const string ParamsValidation = "{ParamsValidation}";

			public const string WrappedCall = "{WrappedCall}";

			public const string WrappedParams = "{WrappedParams}";

			public const string ParamName = "{ParamName}";

			public const string ParamType = "{ParamType}";

			public const string ParamDescription = "{ParamDescription}";

			public const string OriginalType = "{OriginalType}";

			public const string Param = "{Param}";

			public const string GeneralParser = "{GeneralParser}";

			public const string TypedParser = "{TypedParser}";

			public const string FormalParams = "{FormalParams}";

			public const string FirstParam = "{FirstParam}";
		}

		public const string Comma = ",";

		public const string Void = "void";
	}
}
