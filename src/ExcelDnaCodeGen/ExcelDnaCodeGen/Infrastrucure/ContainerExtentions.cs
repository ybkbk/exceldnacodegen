﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;

namespace ExcelDnaCodeGen.Infrastrucure
{
	public static class ContainerExtentions
	{
		private static readonly List<Type> _decoratedChain = new List<Type>();
		private static bool _decoratorChainIsSinglet;

		public static IUnityContainer Singleton<T>(this IUnityContainer ioC)
		{
			return ioC.RegisterType<T>(new ContainerControlledLifetimeManager());
		}

		public static IUnityContainer Instance<T>(this IUnityContainer ioC)
		{
			return ioC.RegisterInstance(typeof(T));
		}

		public static IUnityContainer DecoratorChain<TFrom>(this IUnityContainer ioC, bool singleton = true)
		{
			_decoratorChainIsSinglet = singleton;
			_decoratedChain.Clear();

			return Decorator<TFrom>(ioC);
		}

		public static IUnityContainer Decorator<TFrom>(this IUnityContainer ioC)
		{
			_decoratedChain.Add(typeof(TFrom));
			return ioC;
		}

		public static IUnityContainer EndChain(this IUnityContainer ioC)
		{
			var baseType = _decoratedChain.First();

			for (int i = 1; i < _decoratedChain.Count; i++)
			{
				if (i == 1)
				{
					RegisterNamedType(ioC, baseType, _decoratedChain[i]);
				}
				else if (i == _decoratedChain.Count - 1)
				{
					RegisterDefaultType(ioC, baseType, _decoratedChain[i], GetSpecificName(baseType, _decoratedChain[i - 1]));
				}
				else
				{
					RegisterNamedType(ioC, baseType, _decoratedChain[i], GetSpecificName(baseType, _decoratedChain[i - 1]));
				}
			}

			return ioC;
		}

		private static ResolvedParameter[] GetConstructorParameters(Type typeFrom, Type typeTo, string specificName)
		{
			var parameters = new List<ResolvedParameter>
                                 {
                                     new ResolvedParameter(typeFrom, specificName)
                                 };
			parameters.AddRange(
				typeTo
					.GetConstructors().Single()
					.GetParameters()
					.Where(p => !typeFrom.IsAssignableFrom(p.ParameterType))
					.Select(p => new ResolvedParameter(p.ParameterType)));

			return parameters.ToArray();
		}

		private static void RegisterNamedType(IUnityContainer ioC, Type typeFrom, Type typeTo)
		{
			ioC.RegisterType(
				typeFrom,
				typeTo,
				GetSpecificName(typeFrom, typeTo));
		}

		private static void RegisterNamedType(IUnityContainer ioC, Type typeFrom, Type typeTo, string specificName)
        {
            var parameters = GetConstructorParameters(typeFrom, typeTo, specificName);

            ioC.RegisterType(
                typeFrom,
                typeTo,
                GetSpecificName(typeFrom, typeTo),
                new InjectionConstructor(parameters));
        }

		private static void RegisterDefaultType(IUnityContainer ioC, Type typeFrom, Type typeTo, string decoratedName)
		{
			var parameters = GetConstructorParameters(typeFrom, typeTo, decoratedName);

			if (_decoratorChainIsSinglet)
			{
				ioC.RegisterType(
					typeFrom,
					typeTo,
					new ContainerControlledLifetimeManager(),
					new InjectionConstructor(parameters));
			}
			else
			{
				ioC.RegisterType(
					typeFrom,
					typeTo,
					new InjectionConstructor(parameters));
			}
		}

		private static string GetSpecificName(Type typeFrom, Type typeTo)
		{
			return string.Format("{0}-{1}", typeFrom.Name, typeTo.Name);
		}
	}
}
