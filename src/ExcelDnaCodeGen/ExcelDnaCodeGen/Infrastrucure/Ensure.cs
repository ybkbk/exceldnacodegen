﻿using System;
using System.Linq.Expressions;

namespace ExcelDnaCodeGen.Infrastrucure
{
	public class Ensure
	{
		public static void NotNull(Expression<Func<object>> expr)
		{
			var popertyName = GetPropertyName(expr);

			if (expr.Compile().Invoke() == null)
			{
				throw new ArgumentNullException(popertyName);
			}
		}

		private static string GetPropertyName(Expression<Func<object>> expr)
		{
			var argumentInfo = (expr.Body as MemberExpression).Member;

			return argumentInfo.Name;
		}
	}
}
