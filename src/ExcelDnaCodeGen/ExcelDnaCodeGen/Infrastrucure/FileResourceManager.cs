﻿using System.Collections.Concurrent;
using System.IO;

namespace ExcelDnaCodeGen.Infrastrucure
{
	public class FileResourceManager
	{
		private const string ResourceDir = "Resources";
		private static ConcurrentDictionary<string, string> _resources = new ConcurrentDictionary<string, string>();

		public static string GetFile(string fileName)
		{
			return _resources.GetOrAdd(fileName, ReadFile);
		}

		private static string ReadFile(string fileName)
		{
			return File.ReadAllText(Path.Combine(ResourceDir, fileName));
		}
	}
}
