﻿using System;
using ExcelDnaCodeGen.Infrastrucure;
using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Rules
{
	public class WcfServiceToCppCliRule : IRule
	{
		public string DirectoryName { get { return "cpp_cli"; } }

		public string GetType(DataType type)
		{
			switch (type)
			{
				case DataType.Integer: return "int";
				case DataType.Boolean: return "bool";
				default: throw new ArgumentException("Undefined input type");
			}
		}

		public string GetFileName(string className)
		{
			return string.Format("CppCli{0}.cpp", className);
		}

		public string GetClassTemplate()
		{
			return FileResourceManager.GetFile("CppCliClassTemplate.txt");
		}

		public string GetMethodTemplate()
		{
			return FileResourceManager.GetFile("CppCliMethodTemplate.txt");
		}
	}
}
