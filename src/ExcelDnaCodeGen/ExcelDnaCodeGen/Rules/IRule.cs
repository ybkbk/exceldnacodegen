﻿using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Rules
{
	public interface IRule
	{
		string DirectoryName { get; }

		string GetType(DataType type);

		string GetClassTemplate();

		string GetMethodTemplate();

		string GetFileName(string className);
	}
}
