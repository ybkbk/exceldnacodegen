﻿using System;
using ExcelDnaCodeGen.Infrastrucure;
using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Rules
{
	public class CppCliToNativeRule : IRule
	{
		public string DirectoryName { get { return "cpp_native"; } }

		public string GetType(DataType type)
		{
			switch (type)
			{
				case DataType.Integer: return "int";
				case DataType.Boolean: return "bool";
				default: throw new ArgumentException("Undefined input type");
			}
		}

		public string GetFileName(string className)
		{
			return className + ".cpp";
		}

		public string GetClassTemplate()
		{
			return FileResourceManager.GetFile("NativeCppClassTemplate.txt");
		}

		public string GetMethodTemplate()
		{
			return FileResourceManager.GetFile("NativeCppMethodTemplate.txt");
		}
	}
}
