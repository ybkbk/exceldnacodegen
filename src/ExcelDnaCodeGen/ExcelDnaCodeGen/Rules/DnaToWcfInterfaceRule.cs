﻿using System;
using ExcelDnaCodeGen.Infrastrucure;
using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Rules
{
	public class DnaToWcfInterfaceRule : IRule
	{
		public string DirectoryName { get { return "wcf"; } }

		public string GetType(DataType type)
		{
			switch (type)
			{
				case DataType.Integer: return "int";
				case DataType.Boolean: return "bool";
				case DataType.String: return "string";
				default: throw new ArgumentException("Undefined input type");
			}
		}

		public string GetFileName(string className)
		{
			return string.Format("I{0}.cs", className);
		}

		public string GetClassTemplate()
		{
			return FileResourceManager.GetFile("WcfInterfaceClassTemplate.txt");
		}

		public string GetMethodTemplate()
		{
			return FileResourceManager.GetFile("WcfInterfaceMethodTemplate.txt");
		}
	}
}
