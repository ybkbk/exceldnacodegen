﻿using ExcelDnaCodeGen.Metadata;
using System;
using System.Collections.Generic;
using ExcelDnaCodeGen.Infrastrucure;
using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Rules
{
	public class VbaToDnaRule : IRule
	{
		public string DirectoryName { get { return "excelDna"; } }

		private readonly Dictionary<string, string> _codeFormats = new Dictionary<string, string>()
			{
				{Constants.PlaceHolders.ParamsComment, string.Format("\r\n/// <param name=\"{0}\">{1}</param>", Constants.PlaceHolders.ParamName, Constants.PlaceHolders.ParamDescription)},
				{Constants.PlaceHolders.ReturnComment, string.Format("\r\n/// <returns>{0}</returns>", Constants.PlaceHolders.ReturnValueDescription)},
				{Constants.PlaceHolders.Param, string.Format("\r\n [ExcelArgument(Description=\"{0}\")]\r\n {1} {2}", Constants.PlaceHolders.ParamDescription, Constants.PlaceHolders.ParamType, Constants.PlaceHolders.ParamName)},
				{Constants.PlaceHolders.GeneralParser, string.Format("ExcelAssistant.ParseObjectCode({0})", Constants.PlaceHolders.ParamName)},
				{Constants.PlaceHolders.TypedParser, string.Format("ExcelAssistant.ExcelCellToType<{0}>({1})", Constants.PlaceHolders.OriginalType, Constants.PlaceHolders.ParamName)}
			};

		private readonly Dictionary<DataType, string> _typedValidators = new Dictionary<DataType, string>()
			{
				{DataType.String, string.Format("Contract.Requires(!String.IsNullOrEmpty({0}), \"@ The parameter {1} is mandatory and cannot be null\");", Constants.PlaceHolders.ParamName, Constants.PlaceHolders.ParamDescription)},
				{DataType.Integer, string.Format("Contract.Requires({0} != 0, \"@ The parameter {1} is mandatory and cannot be null\");", Constants.PlaceHolders.ParamName, Constants.PlaceHolders.ParamDescription)}
			};

		public string GetCodeFormatForPlaceHolder(string placeHolder)
		{
			if (!_codeFormats.ContainsKey(placeHolder))
			{
				throw new ArgumentException(string.Format("Couldn't find format for place holder {0}", placeHolder));
			}

			return _codeFormats[placeHolder];
		}

		public string GetValidatorForType(DataType type)
		{
			if (!_typedValidators.ContainsKey(type))
			{
				throw new ArgumentException(string.Format("Couldn't find validator for the type {0}", type));
			}

			return _typedValidators[type];
		}

		public string GetType(DataType type)
		{
			switch (type)
			{
				case DataType.Integer: return "int";
				case DataType.Boolean: return "bool";
				case DataType.String: return "string";
				case DataType.Object: return "object";
				default: throw new ArgumentException("Undefined input type");
			}
		}

		public string GetFileName(string className)
		{
			return className + ".cs";
		}

		public string GetClassTemplate()
		{
			return FileResourceManager.GetFile("ExcelDnaClassTemplate.txt");
		}

		public string GetMethodTemplate()
		{
			return FileResourceManager.GetFile("ExcelDnaMethodTemplate.txt");
		}
	}
}
