﻿using ExcelDnaCodeGen.Metadata;

namespace ExcelDnaCodeGen.Parsers
{
	public interface IBasicMetadataProvider
	{
		VbaToDnaMetadata GetBasicMetadata(string souce);
	}
}
