﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Parsers.Vba
{
	static class AttributesUtility
	{
		public static string GetAttributeValue(IEnumerable<KeyValuePair<string, string>> attributes, string attributeName)
		{
			return (from attribute in attributes where attribute.Key.Equals(attributeName, StringComparison.InvariantCultureIgnoreCase) select attribute.Value).FirstOrDefault();
		}

		public static T ApplyAttributes<T>(List<KeyValuePair<string, string>> attributes, T item)
		{
			var type = typeof (T);
			foreach (var property in type.GetProperties())
			{
				var attribute = (InitFromAttributeAttribute)Attribute.GetCustomAttribute(property, typeof(InitFromAttributeAttribute));
				if (attribute != null)
				{
					var value = GetAttributeValue(attributes, attribute.Name);
					if (value == null && attribute.IsRequired)
					{
						var objectName = type.GetProperty("Name");
						throw new ArgumentException(string.Format("Attribute {0} is required{1}", attribute.Name,
																objectName == null ? string.Empty : string.Format(" for {0}", objectName.GetValue(item))));
					}

					if (value != null)
					{
						if (property.PropertyType.IsAssignableFrom(typeof (string)))
						{
							property.SetValue(item, value);
						}
						else if (property.PropertyType.IsEnum || property.PropertyType.IsNullableEnum())
						{
							property.SetValue(item, ParsingUtils.ParseStringAsEnumMember(property.PropertyType, value, false).Result);
						}
					}
				}
			}

			return item;
		}
	}
}
