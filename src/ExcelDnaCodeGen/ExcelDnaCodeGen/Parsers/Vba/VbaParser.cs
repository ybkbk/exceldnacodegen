﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ExcelDnaCodeGen.Metadata;
using ExcelDnaCodeGen.Metadata.Types;

namespace ExcelDnaCodeGen.Parsers.Vba
{
	public class VbaParser : IBasicMetadataProvider
	{
		private static readonly Regex AttributeRegex = new Regex(@"\[(?<name>\S*?)\s*?\(""(?<value>.*?)""\)\]", RegexOptions.Compiled);
		private static readonly Regex MethodRegex = new Regex(@"(?<name>\S+?)\s*?\(", RegexOptions.Compiled);
		private static readonly Regex ArgumentRegex = new Regex(@"(?<optional>Optional)?\s*(?<passing>ByVal|ByRef)?\s*(?<name>\w+)(?<array>\(\,*\))?\s+as\s+(?<type>\w+)\s*(?<array>\(,*\))?(\s*=\s*(?<value>[^\,]+))?", RegexOptions.Compiled);

		public VbaToDnaMetadata GetBasicMetadata(string souce)
		{
			var content = GetFileContent(souce);
			var metadata = new ClassMetadata {Name = Path.GetFileNameWithoutExtension(souce)};
			var methods = content.Split(new[] {"end\r\n"}, StringSplitOptions.RemoveEmptyEntries);
			metadata.Methods = methods.Select(m => ParseDeclaration(new TextHolder(m))).ToList();
			return new VbaToDnaMetadata {Class = metadata};
		}

		private MethodMetadata ParseDeclaration(TextHolder holder)
		{
			var methodAttributes = ParseAttributes(holder);
			var scope = ParseMethodScope(holder);
			var methodType = ParsingUtils.ParseAsEnumMember<MethodType>(holder, false).Result;
			var method = ParseMethod(holder, methodType);
			method.IsStatic = scope.IsStatic;
			method.Scope = scope.Scope;

			return AttributesUtility.ApplyAttributes(methodAttributes, method);
		}

		private MethodMetadata ParseMethod(TextHolder holder, MethodType methodType)
		{
			var result = new MethodMetadata();
			var methodDeclaration = holder.GetNextPart();
			holder.Move(methodDeclaration.Length);
			string argsDeclaration;
			result.Name = GetMethodName(methodDeclaration, out argsDeclaration);
			result.Parameters = ParseArguments(argsDeclaration);
			if (methodType == MethodType.Function)
			{
				result.ReturnValue = ParseReturnValue(holder, result.Name);
			}

			return result;
		}

		private ParameterMetadata ParseReturnValue(TextHolder holder, string methodName)
		{
			var attributes = ParseAttributes(holder);
			if (holder.Length != 0)
			{
				holder.Move(holder.GetNextPart().Length);
				if (holder.Length != 0)
				{
					var result = new ParameterMetadata {ParameterType = ParsingUtils.ParseAsEnumMember<DataType>(holder, false).Result};
					return AttributesUtility.ApplyAttributes(attributes, result);
				}
			}

			throw new ArgumentException(string.Format("Return type is not specified for the function {0}", methodName));
		}

		private List<ParameterMetadata> ParseArguments(string argsDeclaration)
		{
			var holder = new TextHolder(argsDeclaration.Substring(1, argsDeclaration.Length - 2));
			var parameters = new List<ParameterMetadata>();
			while (holder.Length != 0)
			{
				var attributes = ParseAttributes(holder);
				var parameter = ParseArgument(holder);
				parameters.Add(AttributesUtility.ApplyAttributes(attributes, parameter));
			}

			return parameters;
		}

		private ParameterMetadata ParseArgument(TextHolder declaration)
		{
			var result = ArgumentRegex.Match(declaration.ToString());
			if (!result.Success)
			{
				throw new ArgumentException(string.Format("Invalid arguments declartion near {0}", declaration));
			}

			declaration.Move(result.Length);
			if (declaration.Length != 0)
			{
				//skip comma
				declaration.Move();
			}

			return BuildParameterFromMatch(result);
		}

		private ParameterMetadata BuildParameterFromMatch(Match parsingResult)
		{
			var result = new ParameterMetadata();
			var isArray = parsingResult.Groups[RegexGroupName.Array].Success;
			result.ParameterType = ParsingUtils.ParseStringAsEnumMember<DataType>(parsingResult.Groups[RegexGroupName.Type].Value, false).Result;
			if (isArray)
			{
				result = new ArrayParameter
					{
						ElementType = result.ParameterType,
						DementialsCount = parsingResult.Groups[RegexGroupName.Array].Value.Count(c => c == ',') + 1,
						ParameterType = DataType.Array
					};
			}

			result.Name = parsingResult.Groups[RegexGroupName.Name].Value;
			result.DefaultValue = parsingResult.Groups[RegexGroupName.Value].Success
									? parsingResult.Groups[RegexGroupName.Value].Value
									: null;
			result.PassingMechanism =
				ParsingUtils.ParseStringAsEnumMember<ParameterPassingMechanism>(parsingResult.Groups[RegexGroupName.Passing].Value).Result;

			return result;
		}

		private string GetMethodName(string methodDeclaration, out string argsDeclaration)
		{
			var match = MethodRegex.Match(methodDeclaration);
			if (!match.Success)
			{
				throw new ArgumentException(string.Format("Method has invalid declaration: {0}", methodDeclaration));
			}

			argsDeclaration = methodDeclaration.Substring(match.Groups[RegexGroupName.Name].Value.Length);
			return match.Groups[RegexGroupName.Name].Value;
		}

		private ScopedItem ParseMethodScope(TextHolder holder)
		{
			var result = new ScopedItem();
			for (int i = 0; i < 2; i++)
			{
				var scope = ParsingUtils.ParseAsEnumMember<VbaScope>(holder);
				if (scope.IsSuccess)
				{
					result.Scope = (Scope)scope.Result;
				}
				else
				{
					var staticParsing = ParseStatic(holder);
					if (staticParsing.IsSuccess)
					{
						result.IsStatic = staticParsing.Result;
					}
				}
			}

			return result;
		}

		private ParsingResult<bool> ParseStatic(TextHolder holder)
		{
			var part = holder.GetNextPart();
			if (part.Equals("static", StringComparison.InvariantCultureIgnoreCase))
			{
				holder.Move(part.Length);
				return new ParsingResult<bool> {IsSuccess = true, Result = true};
			}
			
			return new ParsingResult<bool>();
		}

		private List<KeyValuePair<string, string>> ParseAttributes(TextHolder holder)
		{
			var result = new List<KeyValuePair<string, string>>();
			var parsedAttribute = ParseAttribute(holder);

			while(parsedAttribute.IsSuccess)
			{
				result.Add(parsedAttribute.Result);
				parsedAttribute = ParseAttribute(holder);
			}

			return result;
		}

		private ParsingResult<KeyValuePair<string, string>> ParseAttribute(TextHolder holder)
		{
			var part = holder.GetNextPart();
			var match = AttributeRegex.Match(part);
			
			if (match.Success && match.Groups.Count == 3)
			{
				holder.Move(part.Length);
				return new ParsingResult<KeyValuePair<string, string>>
					{
						IsSuccess = true,
						Result = new KeyValuePair<string, string>(match.Groups[RegexGroupName.Name].Value, match.Groups[RegexGroupName.Value].Value)
					};
			}

			return new ParsingResult<KeyValuePair<string, string>>();
		}

		private string GetFileContent(string file)
		{
			if (!File.Exists(file))
			{
				throw new FileNotFoundException(string.Format("File {0} doesn't exist.", file));
			}

			using (var reader = new StreamReader(File.OpenRead(file)))
			{
				return reader.ReadToEnd();
			}
		}
	}
}
