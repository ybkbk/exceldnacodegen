﻿namespace ExcelDnaCodeGen.Parsers.Vba
{
	enum VbaScope
	{
		Global = 0,
		Public = 0,
		Private = 1,
		Friend = 2
	}
}
