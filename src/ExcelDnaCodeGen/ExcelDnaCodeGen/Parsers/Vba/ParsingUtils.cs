﻿using System;
using System.Text;

namespace ExcelDnaCodeGen.Parsers.Vba
{
	static class ParsingUtils
	{
		public static string EnumerateEnum<T>()
		{
			var builder = new StringBuilder();
			foreach (T item in (T[])Enum.GetValues(typeof(T)))
			{
				builder.Append(item);
				builder.Append(' ');
			}

			builder.Remove(builder.Length - 1, 1);
			return builder.ToString();
		}

		public static string EnumerateEnum(Type enumType)
		{
			var builder = new StringBuilder();
			foreach (var item in Enum.GetValues(enumType))
			{
				builder.Append(item);
				builder.Append(' ');
			}

			builder.Remove(builder.Length - 1, 1);
			return builder.ToString();
		}

		public static ParsingResult<T> ParseStringAsEnumMember<T>(string text, bool returnDefault = true) where T : struct
		{
			T item;
			if (!string.IsNullOrEmpty(text) && Enum.TryParse(text, true, out item))
			{
				return new ParsingResult<T> { IsSuccess = true, Result = item };
			}

			if (returnDefault)
			{
				return new ParsingResult<T>();
			}

			throw new ArgumentException(string.Format("Value {0} couldn't be parsed, allowed values: {1}", text, EnumerateEnum<T>()));
		}

		public static ParsingResult<object> ParseStringAsEnumMember(Type enumType, string text, bool returnDefault = true)
		{
			try
			{
				if (enumType.IsNullableEnum())
				{
					enumType = Nullable.GetUnderlyingType(enumType);
				}

				if (!string.IsNullOrEmpty(text))
				{
					var result = Enum.Parse(enumType, text, true);
					return new ParsingResult<object> { IsSuccess = true, Result = result };
				}
			}
			catch (Exception)
			{
				// throw custom exception in case of parsing error
			}

			if (returnDefault)
			{
				return new ParsingResult<object> { Result = Activator.CreateInstance(enumType) };
			}

			throw new ArgumentException(string.Format("Value {0} couldn't be parsed, allowed values: {1}", text, EnumerateEnum(enumType)));
		}

		public static ParsingResult<T> ParseAsEnumMember<T>(TextHolder holder, bool returnDefault = true) where T : struct
		{
			var part = holder.GetNextPart();
			var result = ParseStringAsEnumMember<T>(part, returnDefault);
			if (result.IsSuccess)
			{
				holder.Move(part.Length);
			}

			return result;
		}

		public static bool IsNullableEnum(this Type t)
		{
			Type u = Nullable.GetUnderlyingType(t);
			return (u != null) && u.IsEnum;
		}
	}
}
