﻿namespace ExcelDnaCodeGen.Parsers.Vba
{
	static class RegexGroupName
	{
		public const string Name = "name";
		public const string Value = "value";
		public const string Optional = "optional";
		public const string Passing = "passing";
		public const string Array = "array";
		public const string Type = "type";
	}
}
