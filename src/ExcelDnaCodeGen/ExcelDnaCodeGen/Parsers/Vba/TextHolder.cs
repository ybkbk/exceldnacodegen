﻿using System;
using System.Collections.Generic;

namespace ExcelDnaCodeGen.Parsers.Vba
{
	public class TextHolder
	{
		private readonly Dictionary<string,string> _separators = new Dictionary<string, string>(){{"[","]"}, {"(", ")"}, {"\"","\""}};
		private const string SpaceSeparator = " ";
		private readonly string _sorce;
		private int _position;

		public TextHolder(string source)
		{
			_sorce = source.Replace("\r\n", " ").Replace("\t", " ").Replace("\r", " ").Replace("\n", " ").Trim();
			_position = 0;
		}

		public int Length
		{
			get { return _sorce.Length - _position; }
		}

		public int OriginalLength
		{
			get { return _sorce.Length; }
		}

		public bool IsEndLine
		{
			get { return _position == OriginalLength; }
		}

		public TextHolder Move(int step = 1)
		{
			if (step < 0 || step > _sorce.Length - _position)
			{
				throw new ArgumentOutOfRangeException("step", string.Format("Should be between 0 and {0}", Length));
			}

			_position += step;
			return this;
		}

		public TextHolder Set(int position)
		{
			if (position < 0 || position > _sorce.Length)
			{
				throw new ArgumentOutOfRangeException("position", string.Format("Should be between 0 and {0}", _sorce.Length));
			}

			_position = position;
			return this;
		}

		public override string ToString()
		{
			return _sorce.Substring(_position);
		}

		public string GetNextPart()
		{
			SkipAllLeadingSpaces();

			int currentPosiotion;
			var partSepartors = new Stack<string>();

			for (currentPosiotion = _position; currentPosiotion < OriginalLength; currentPosiotion++)
			{
				var ch = _sorce[currentPosiotion].ToString();
				if (partSepartors.Count != 0 && partSepartors.Peek() == ch)
				{
					partSepartors.Pop();
					if (partSepartors.Count == 0)
					{
						currentPosiotion++;
						break;
					}
				}
				else if (_separators.ContainsKey(ch))
				{
					partSepartors.Push(_separators[ch]);
				} 

				if(ch == SpaceSeparator && partSepartors.Count == 0)
				{
					break;
				}
			}

			if (currentPosiotion == OriginalLength && partSepartors.Count != 0)
			{
				throw new ArgumentException(string.Format("Text has ivalid structure near {0}", this));
			}

			return _sorce.Substring(_position, currentPosiotion - _position);
		}

		private void SkipAllLeadingSpaces()
		{
			for (; _position < OriginalLength && SpaceSeparator.Equals(_sorce[_position].ToString()); _position++) ;
		}
	}
}
