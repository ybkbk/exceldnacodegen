﻿namespace ExcelDnaCodeGen.Parsers.Vba
{
	class ParsingResult<T>
	{
		public T Result { get; set; }
		public bool IsSuccess { get; set; }
	}
}
